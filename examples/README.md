# GoDS (Go Data Structures)

Various examples on how to use data structures.

## Examples

- [ArrayList](https://gitee.com/lhjw9810/gods/blob/master/examples/arraylist/arraylist.go)
- [ArrayStack](https://gitee.com/lhjw9810/gods/blob/master/examples/arraystack/arraystack.go)
- [AVLTree](https://gitee.com/lhjw9810/gods/blob/master/examples/avltree/avltree.go)
- [BinaryHeap](https://gitee.com/lhjw9810/gods/blob/master/examples/binaryheap/binaryheap.go)
- [BTree](https://gitee.com/lhjw9810/gods/blob/master/examples/btree/btree.go)
- [Custom Comparator](https://gitee.com/lhjw9810/gods/blob/master/examples/customcomparator/customcomparator.go)
- [DoublyLinkedList](https://gitee.com/lhjw9810/gods/blob/master/examples/doublylinkedlist/doublylinkedlist.go)
- [EnumerableWithIndex](https://gitee.com/lhjw9810/gods/blob/master/examples/enumerablewithindex/enumerablewithindex.go)
- [EnumerableWithKey](https://gitee.com/lhjw9810/gods/blob/master/examples/enumerablewithkey/enumerablewithkey.go)
- [HashBidiMap](https://gitee.com/lhjw9810/gods/blob/master/examples/hashbidimap/hashbidimap.go)
- [HashMap](https://gitee.com/lhjw9810/gods/blob/master/examples/hashmap/hashmap.go)
- [HashSet](https://gitee.com/lhjw9810/gods/blob/master/examples/hashset/hashset.go)
- [IteratorWithIndex](https://gitee.com/lhjw9810/gods/blob/master/examples/iteratorwithindex/iteratorwithindex.go)
- [iteratorwithkey](https://gitee.com/lhjw9810/gods/blob/master/examples/iteratorwithkey/iteratorwithkey.go)
- [IteratorWithKey](https://gitee.com/lhjw9810/gods/blob/master/examples/linkedliststack/linkedliststack.go)
- [RedBlackTree](https://gitee.com/lhjw9810/gods/blob/master/examples/redblacktree/redblacktree.go)
- [RedBlackTreeExtended](https://gitee.com/lhjw9810/gods/blob/master/examples/redblacktreeextended/redblacktreeextended.go)
- [Serialization](https://gitee.com/lhjw9810/gods/blob/master/examples/serialization/serialization.go)
- [SinglyLinkedList](https://gitee.com/lhjw9810/gods/blob/master/examples/singlylinkedlist/singlylinkedlist.go)
- [Sort](https://gitee.com/lhjw9810/gods/blob/master/examples/sort/sort.go)
- [TreeBidiMap](https://gitee.com/lhjw9810/gods/blob/master/examples/treebidimap/treebidimap.go)
- [TreeMap](https://gitee.com/lhjw9810/gods/blob/master/examples/treemap/treemap.go)
- [TreeSet](https://gitee.com/lhjw9810/gods/blob/master/examples/treeset/treeset.go)
